package com.stevenhia.testassessmentnewsapp.network

import com.stevenhia.testassessmentnewsapp.BuildConfig
import com.stevenhia.testassessmentnewsapp.model.ArticleResponse
import com.stevenhia.testassessmentnewsapp.model.SourceResponse
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

class RetrofitConfig {
    companion object {
        private fun loggingInterceptor(): Interceptor {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            return httpLoggingInterceptor
        }

        private fun okHttpClientBuilder(builder: OkHttpClient.Builder): OkHttpClient.Builder {
            return builder.addInterceptor(loggingInterceptor())
        }

        private fun provideClient(): OkHttpClient {
            return okHttpClientBuilder(OkHttpClient.Builder())
                .retryOnConnectionFailure(true)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build()
        }

        fun getAPIService(): APIService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(provideClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(APIService::class.java)
        }
    }
}

interface APIService {
    @GET("top-headlines/sources?apiKey=${BuildConfig.API_KEY}")
    fun getSourceNewsByCategory(@Query("category") category: String): Call<SourceResponse>

    @GET("top-headlines?apiKey=${BuildConfig.API_KEY}")
    fun getArticleNewsBySource(@Query("sources") sources: String): Call<ArticleResponse>

    @GET("top-headlines?apiKey=${BuildConfig.API_KEY}")
    fun getDataSearch(@Query("q") query: String): Call<ArticleResponse>
}
