package com.stevenhia.testassessmentnewsapp.ui.article

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.addCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.stevenhia.testassessmentnewsapp.R
import com.stevenhia.testassessmentnewsapp.adapter.ArticleAdapter
import com.stevenhia.testassessmentnewsapp.databinding.ActivityArticleBinding
import com.stevenhia.testassessmentnewsapp.viewmodel.ArticleViewModel

class ArticleActivity : AppCompatActivity() {
    private lateinit var binding: ActivityArticleBinding
    private lateinit var articleAdapter: ArticleAdapter

    private val articleViewModel: ArticleViewModel by viewModels()
    private var data: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArticleBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setToolbar()

        articleAdapter = ArticleAdapter()
        showLoading(true)
        emptyData(false)

        data = intent.getStringExtra(INTENT_DATA)
        data?.let { setViewModel(it) }

        onBackPressedDispatcher.addCallback(this) {
            finish()
        }
    }

    private fun setToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setViewModel(data: String) {
        articleViewModel.getDataAPI(data).observe(this) { article ->
            showLoading(true)
            if (article != null) {
                articleAdapter.setData(article)
                Log.d("CHECK SOURCE", article.toString())
                setRecyclerView()
                showLoading(false)
                emptyData(false)
            } else {
                showLoading(false)
                emptyData(true)
            }
        }
        articleViewModel.getError().observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
    }

    private fun setRecyclerView() {
        binding.rvArticle.setHasFixedSize(true)
        binding.rvArticle.layoutManager = LinearLayoutManager(this)
        binding.rvArticle.adapter = articleAdapter
    }

    private fun showLoading(progress: Boolean) {
        if (progress) {
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.GONE
        }
    }

    private fun emptyData(empty: Boolean) {
        if (empty) {
            binding.textError.text = getString(R.string.empty_data)
            binding.textError.visibility = View.VISIBLE
            binding.animationView.visibility = View.VISIBLE
        } else {
            binding.textError.visibility = View.GONE
            binding.animationView.visibility = View.GONE
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    companion object {
        const val INTENT_DATA = "intent_data"
    }
}