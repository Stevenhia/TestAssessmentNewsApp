package com.stevenhia.testassessmentnewsapp.ui.second

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.stevenhia.testassessmentnewsapp.R
import com.stevenhia.testassessmentnewsapp.adapter.SourceAdapter
import com.stevenhia.testassessmentnewsapp.databinding.ActivitySourceBinding
import com.stevenhia.testassessmentnewsapp.viewmodel.SourceViewModel

class SourceActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySourceBinding
    private lateinit var sourceAdapter: SourceAdapter

    private val sourceViewModel: SourceViewModel by viewModels()

    private var category: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySourceBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setToolbar()

        showLoading(true)
        category = intent.getStringExtra(INTENT_CATEGORY)
        Log.d("CHECK INTENT", category.toString())
        if (category != null) {
            setViewModel(category!!)
        }
        sourceAdapter = SourceAdapter()

    }

    private fun setToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = getString(R.string.title_source)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setViewModel(category: String) {
        sourceViewModel.getNewsSource(category).observe(this) { data ->
            if (data != null) {
                sourceAdapter.setData(data)
                setRecyclerView()
                showLoading(false)
            }
        }
        sourceViewModel.getError().observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
    }

    private fun setRecyclerView() {
        binding.rvSource.layoutManager = LinearLayoutManager(this)
        binding.rvSource.setHasFixedSize(true)
        binding.rvSource.adapter = sourceAdapter
    }

    private fun showLoading(progress: Boolean) {
        if (progress) {
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.GONE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    companion object {
        const val INTENT_CATEGORY = "intent_category"
    }
}