package com.stevenhia.testassessmentnewsapp.ui.search

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.stevenhia.testassessmentnewsapp.R
import com.stevenhia.testassessmentnewsapp.adapter.ArticleAdapter
import com.stevenhia.testassessmentnewsapp.databinding.ActivitySearchBinding
import com.stevenhia.testassessmentnewsapp.viewmodel.SearchViewModel

class SearchActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySearchBinding
    private val mainViewModels: SearchViewModel by viewModels()
    private var adapter: ArticleAdapter = ArticleAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setToolbar()

        showLoading(false)
        setSearchView()
        setViewModel()
    }

    private fun setToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun setViewModel() {
        mainViewModels.getDataAPI().observe(this) { data ->
            showLoading(true)
            if (data != null) {
                adapter.setData(data)
                showRecyclerView()
                showLoading(false)
            }
        }
        mainViewModels.getError().observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
    }

    private fun setSearchView() {
        binding.searchView.requestFocus()
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isEmpty()) {
                    showLoading(false)
                } else {
                    showLoading(false)
                    mainViewModels.setSearchData(newText)
                }
                return false
            }
        })
    }

    private fun showRecyclerView() {
        binding.rvNews.setHasFixedSize(true)
        binding.rvNews.layoutManager = LinearLayoutManager(this)
        binding.rvNews.adapter = adapter
    }

    private fun showLoading(progress: Boolean) {
        if (progress) {
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}