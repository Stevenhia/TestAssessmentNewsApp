package com.stevenhia.testassessmentnewsapp.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.stevenhia.testassessmentnewsapp.adapter.CategoryAdapter
import com.stevenhia.testassessmentnewsapp.databinding.ActivityMainBinding
import com.stevenhia.testassessmentnewsapp.ui.search.SearchActivity
import com.stevenhia.testassessmentnewsapp.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private lateinit var adapterCategory: CategoryAdapter
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setToolbar()

        adapterCategory = CategoryAdapter()

        setViewModel()
        setRecyclerView()

        binding.intentSearch.setOnClickListener {
            startActivity(Intent(this, SearchActivity::class.java))
        }
    }

    private fun setToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun setViewModel() {
        mainViewModel.getCategory(this).observe(this) { category ->
            if (category != null) {
                adapterCategory.setData(category)
            }
        }
    }

    private fun setRecyclerView() {
        binding.rvCategory.layoutManager =
            GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        binding.rvCategory.setHasFixedSize(true)
        binding.rvCategory.adapter = adapterCategory
    }
}