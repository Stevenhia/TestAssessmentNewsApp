package com.stevenhia.testassessmentnewsapp.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.stevenhia.testassessmentnewsapp.R
import com.stevenhia.testassessmentnewsapp.databinding.ItemArticleBinding
import com.stevenhia.testassessmentnewsapp.model.ArticleData
import com.stevenhia.testassessmentnewsapp.ui.detail.WebViewActivity
import com.stevenhia.testassessmentnewsapp.utils.DateHelper

class ArticleAdapter : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {
    private var listArticle = ArrayList<ArticleData>()
    fun setData(items: List<ArticleData>) {
        listArticle.clear()
        listArticle.addAll(items)
    }

    class ViewHolder(private val binding: ItemArticleBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(news: ArticleData) {
            with(binding) {
                Glide.with(itemView.context)
                    .load(news.photo)
                    .placeholder(R.drawable.ic_baseline_image_24)
                    .into(imageNews)
                textName.text =
                    itemView.resources.getString(R.string.name_source, news.nameSource.nameSource)
                if (news.author != null && news.author !="") {
                    textAuthor.text = itemView.resources.getString(R.string.author, news.author)
                } else {
                    textAuthor.text = itemView.resources.getString(R.string.author, "Unknown")
                }
                textDescription.text = news.description
                textTitle.text = news.title

                val formatDate = DateHelper.dateFormat(news.publish!!)
                textPublish.text = itemView.resources.getString(R.string.publish, formatDate)

                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, WebViewActivity::class.java)
                    intent.putExtra(WebViewActivity.PAGE_URL, news.urlNews)
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemArticleBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listArticle[position])
    }

    override fun getItemCount(): Int = listArticle.size
}