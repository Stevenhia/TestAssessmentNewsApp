package com.stevenhia.testassessmentnewsapp.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stevenhia.testassessmentnewsapp.R
import com.stevenhia.testassessmentnewsapp.databinding.ItemSourceBinding
import com.stevenhia.testassessmentnewsapp.model.SourceData
import com.stevenhia.testassessmentnewsapp.ui.article.ArticleActivity

class SourceAdapter : RecyclerView.Adapter<SourceAdapter.ViewHolder>() {
    private val listSource = ArrayList<SourceData>()

    fun setData(items: List<SourceData>) {
        listSource.clear()
        listSource.addAll(items)
    }

    class ViewHolder(private val binding: ItemSourceBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: SourceData) {
            binding.tvNameSource.text =
                itemView.context.getString(R.string.get_name_source, data.nameSource)
            binding.tvDescriptionSource.text =
                itemView.context.getString(R.string.get_description_source, data.descriptionSource)
            binding.tvCategorySource.text =
                itemView.context.getString(R.string.get_category_source, data.categorySource)

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, ArticleActivity::class.java)
                intent.putExtra(ArticleActivity.INTENT_DATA, data.idSource)
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemSourceBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listSource[position])
    }

    override fun getItemCount(): Int = listSource.size
}