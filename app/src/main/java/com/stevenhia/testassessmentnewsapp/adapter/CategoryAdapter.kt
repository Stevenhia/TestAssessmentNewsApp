package com.stevenhia.testassessmentnewsapp.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stevenhia.testassessmentnewsapp.databinding.ItemCategoryBinding
import com.stevenhia.testassessmentnewsapp.model.CategoryData
import com.stevenhia.testassessmentnewsapp.ui.second.SourceActivity

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private val listCategory = ArrayList<CategoryData>()

    fun setData(items:List<CategoryData>){
        listCategory.clear()
        listCategory.addAll(items)
    }

    class ViewHolder(private val binding: ItemCategoryBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data:CategoryData){
            binding.tvCategory.text = data.nameCategory

            itemView.setOnClickListener {
                val intent = Intent(itemView.context,SourceActivity::class.java)
                intent.putExtra(SourceActivity.INTENT_CATEGORY,data.nameCategory)
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemCategoryBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listCategory[position])
    }

    override fun getItemCount(): Int = listCategory.size
}