package com.stevenhia.testassessmentnewsapp.model

import com.google.gson.annotations.SerializedName

data class SourceResponse(
    @SerializedName("sources")
    var listSource: List<SourceData>
)

data class SourceData(
    @SerializedName("id")
    var idSource: String? = null,
    @SerializedName("name")
    var nameSource: String? = null,
    @SerializedName("category")
    var categorySource: String? = null,
    @SerializedName("description")
    var descriptionSource: String? = null,
    @SerializedName("url")
    var urlWebsite: String? = null
)
