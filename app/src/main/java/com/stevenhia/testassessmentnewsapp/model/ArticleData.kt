package com.stevenhia.testassessmentnewsapp.model

import com.google.gson.annotations.SerializedName

data class ArticleResponse(
    @field:SerializedName("articles")
    var listCategory: List<ArticleData>
)

data class ArticleData(
    @SerializedName("urlToImage")
    var photo: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("source")
    var nameSource: SourceData,
    @SerializedName("author")
    var author: String? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("publishedAt")
    var publish: String? = null,
    @SerializedName("url")
    var urlNews: String? = null
)
