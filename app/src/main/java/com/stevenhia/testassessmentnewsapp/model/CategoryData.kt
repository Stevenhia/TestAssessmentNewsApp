package com.stevenhia.testassessmentnewsapp.model

import com.google.gson.annotations.SerializedName

data class CategoriesResponse(
    @field:SerializedName("categories")
    var listCategory: List<CategoryData>
)

data class CategoryData(

    @field:SerializedName("id")
    var idCategory: Int? = 0,

    @field:SerializedName("name")
    var nameCategory: String? = null,
)
