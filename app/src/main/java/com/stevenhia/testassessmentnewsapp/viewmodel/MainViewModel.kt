package com.stevenhia.testassessmentnewsapp.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.GsonBuilder
import com.stevenhia.testassessmentnewsapp.model.CategoriesResponse
import com.stevenhia.testassessmentnewsapp.model.CategoryData
import com.stevenhia.testassessmentnewsapp.utils.readJSONFile


class MainViewModel : ViewModel() {
    private val _listCategory = MutableLiveData<List<CategoryData>>()

    private fun fetchCategory(context: Context) {
        val gson = GsonBuilder().setPrettyPrinting().create()
        val dataMenu = gson.fromJson(
            readJSONFile(context, "category.json"),
            CategoriesResponse::class.java
        )

        _listCategory.postValue(dataMenu.listCategory)

    }

    fun getCategory(context: Context): LiveData<List<CategoryData>> {
        fetchCategory(context)
        return _listCategory
    }
}
