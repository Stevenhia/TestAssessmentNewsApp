package com.stevenhia.testassessmentnewsapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stevenhia.testassessmentnewsapp.model.SourceData
import com.stevenhia.testassessmentnewsapp.model.SourceResponse
import com.stevenhia.testassessmentnewsapp.network.RetrofitConfig
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SourceViewModel : ViewModel() {
    private val errorMsg = MutableLiveData<String>()
    private val _listSourceNews = MutableLiveData<List<SourceData>>()

    private fun fetchNewsSource(category: String) {
        RetrofitConfig.getAPIService().getSourceNewsByCategory(category)
            .enqueue(object : Callback<SourceResponse> {
                override fun onResponse(
                    call: Call<SourceResponse>,
                    response: Response<SourceResponse>
                ) {
                    if (response.isSuccessful) {
                        _listSourceNews.postValue(response.body()?.listSource)

                        Log.d("CHECK VIEWMODEL ", response.body()?.listSource.toString())
                    }
                }

                override fun onFailure(call: Call<SourceResponse>, t: Throwable) {
                    errorMsg.value = t.message.toString()
                }
            })
    }

    fun getNewsSource(category: String): LiveData<List<SourceData>> {
        fetchNewsSource(category)
        return _listSourceNews
    }

    fun getError(): LiveData<String> = errorMsg
}