package com.stevenhia.testassessmentnewsapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stevenhia.testassessmentnewsapp.model.ArticleData
import com.stevenhia.testassessmentnewsapp.model.ArticleResponse
import com.stevenhia.testassessmentnewsapp.network.RetrofitConfig
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchViewModel : ViewModel() {
    private val errorMsg = MutableLiveData<String>()
    private val listNews = MutableLiveData<List<ArticleData>>()

    fun setSearchData(query: String) {
        RetrofitConfig.getAPIService().getDataSearch(query)
            .enqueue(object : Callback<ArticleResponse> {
                override fun onResponse(
                    call: Call<ArticleResponse>,
                    response: Response<ArticleResponse>
                ) {
                    if (response.isSuccessful) {
                        listNews.postValue(response.body()?.listCategory)
                    }
                }

                override fun onFailure(call: Call<ArticleResponse>, t: Throwable) {
                    errorMsg.postValue(t.message.toString())
                }
            })
    }

    fun getDataAPI(): LiveData<List<ArticleData>> {
        return listNews
    }

    fun getError(): LiveData<String> = errorMsg
}