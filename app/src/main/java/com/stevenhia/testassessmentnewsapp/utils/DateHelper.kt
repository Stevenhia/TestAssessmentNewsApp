package com.stevenhia.testassessmentnewsapp.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateHelper {
    fun dateFormat(publishedAt: String): String? {
        val newDate: String? = try {
            val date = SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss'Z'",
                Locale.getDefault()
            ).parse(publishedAt)
            SimpleDateFormat("E, dd MMMM yyyy", Locale.US).format(date!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            publishedAt
        }
        return newDate
    }
}